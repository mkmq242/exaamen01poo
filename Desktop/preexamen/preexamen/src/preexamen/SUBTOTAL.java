/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author je270
 */
public class SUBTOTAL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        RECIBO recibopago;
        recibopago = new RECIBO();
        recibopago.setNumContrato(102);
        recibopago.setFecha("21 Marzo 2019");
        recibopago.setNombre("José Lopez");
        recibopago.setDomicilio("Av del sol 1200");
        recibopago.setTipoContrato(1);
        recibopago.setNivelEstudios(1);
        recibopago.setPagBase(700);
        recibopago.setDiasTrabajados(15);
        
        System.out.println("Num. contrato: "+recibopago.getNumContrato());
        System.out.println("Fecha de pago: "+recibopago.getFecha());
        System.out.println("Nombre empleado: "+recibopago.getNombre());
        System.out.println("Domicilio: "+recibopago.getDomicilio());
        System.out.println("Tipo de contrato: "+recibopago.getTipoContrato()+": Temporal");
        System.out.println("Nivel de estudios: "+recibopago.getNivelEstudios()+" Licenciatura");
        System.out.println("Pago base: $"+recibopago.getPagBase());
        System.out.println("Dias trabajados: "+recibopago.getDiasTrabajados());
        System.out.println("Subtotal: $"+recibopago.calcularSubtotal());
        System.out.println("Impuesto: $"+recibopago.calcularImpuesto());
        System.out.println("Total a pagar: $"+recibopago.calcularTotal());
        
        System.out.println("///////////////////////////////////////////////////////////////");
        
          recibopago.setNumContrato(103);
        recibopago.setFecha("203 Marzo 2019");
        recibopago.setNombre("María Acosta");
        recibopago.setDomicilio("Av del sol 1200");
        recibopago.setTipoContrato(2);
        recibopago.setNivelEstudios(2);
        recibopago.setPagBase(700);
        recibopago.setDiasTrabajados(15);
        
        System.out.println("Num. contrato: "+recibopago.getNumContrato());
        System.out.println("Fecha de pago: "+recibopago.getFecha());
        System.out.println("Nombre empleado: "+recibopago.getNombre());
        System.out.println("Domicilio: "+recibopago.getDomicilio());
        System.out.println("Tipo de contrato: "+recibopago.getTipoContrato()+": Base");
        System.out.println("Nivel de estudios: "+recibopago.getNivelEstudios()+" Maestria");
        System.out.println("Pago base: $"+recibopago.getPagBase());
        System.out.println("Dias trabajados: "+recibopago.getDiasTrabajados());
        System.out.println("Subtotal: $"+recibopago.calcularSubtotal());
        System.out.println("Impuesto: $"+recibopago.calcularImpuesto());
        System.out.println("Total a pagar: $"+recibopago.calcularTotal());
        
    }
    
}
