/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author MARTINEZ DURAN MARCOS ULISES
 */
public class RECIBO {
    public int numContrato = 0;
    public String fecha = "";
    public String nombre = "";
    public String domicilio = "";
    public int tipoContrato = 0;
    public int nivelEstudios = 0;
    public float pagBase = 0.0f;
    public int diasTrabajados =0;
    public double incremento;
    
    public RECIBO(){
    this.numContrato=0;
    this.fecha="";
    this.nombre="";
    this.domicilio="";
    this.tipoContrato=0;
    this.nivelEstudios=0;
    this.pagBase=0.0f;
    this.diasTrabajados=0;
    }
    public RECIBO(int numContrato, String fecha, String nombre, String domicilio, int tipoContrato, int nivelEstudios, float pagBase, int diasTrabajados){
    this.numContrato=numContrato;
    this.fecha=fecha;
    this.nombre=nombre;
    this.domicilio=domicilio;
    this.tipoContrato=tipoContrato;
    this.nivelEstudios=nivelEstudios;
    this.pagBase=pagBase;
    this.diasTrabajados=diasTrabajados;
    }
    public RECIBO(RECIBO otro){
     this.numContrato=otro.numContrato;
    this.fecha=otro.fecha;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
    this.tipoContrato=otro.tipoContrato;
    this.nivelEstudios=otro.nivelEstudios;
    this.pagBase=otro.pagBase;
    this.diasTrabajados=otro.diasTrabajados;
    }

    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public int getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(int nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public float getPagBase() {
        return pagBase;
    }

    public void setPagBase(float pagBase) {
        this.pagBase = pagBase;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    public float calcularSubtotal(){
    

     if(nivelEstudios==1){incremento=1.20;}
     if(nivelEstudios==2){incremento=1.50;}
     if(nivelEstudios==3){incremento=2;}
     return (float) (pagBase * incremento * diasTrabajados);

    }
    public float calcularImpuesto(){
    return (float) (calcularSubtotal()*0.16);
    }
    public float calcularTotal(){
        return calcularSubtotal()-calcularImpuesto();
    }
}